package concurrency;

public class UnworkingGuardedBlocks {

  public static int waitIncrement = 0;
  public static boolean flag = false;
  
  public static void main(String[] args) {
    
    Thread dontWaitForMe = new Thread() {
      public void run() {
        for (int i = 0; i < 1000000; i++){
          waitIncrement++;
        };
        flag = true;
      }
    };
    
    Thread iDontWaitToPrint = new Thread() {
      public void run() {
        while (!flag) 
          System.out.println("I'm waiting");
        System.out.println("I have waited to print: " + waitIncrement);
      }
    };

    dontWaitForMe.run();
    iDontWaitToPrint.run();
    
  }
}
