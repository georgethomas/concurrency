package concurrency;

/*   Write a short program in which two threads both increment a shared
     integer repeatedly, without proper synchronisation, 1,000,000 times,
     printing the result at the end of the program.
     
     Now modify the program to use synchronized to ensure that increments
     on the shared variable are atomic.
 */

public class SimpleSync {

  static int incrementMeUnsynched = 0; // Less than 2,000,000.
  static int incrementMeSynched = 0; // Also less than 2,000,000.
  static int incrementMeSynchedActually = 0; // Actually 2,000,000.
  
  public static void main(String[] args) throws InterruptedException {
    
    Thread[] unsyncedThreads = new Thread[2];
    for (int i = 0; i < unsyncedThreads.length; i++) {
      unsyncedThreads[i] = new UnsynchedIncrementor();
    }
    for (Thread t : unsyncedThreads) {
      t.start();
    }
    unsyncedThreads[0].join();
    unsyncedThreads[1].join();
    System.out.println("Unsynced increment two million times gives :" + incrementMeUnsynched);
    
    Thread[] syncedThreads = new Thread[2];
    for (int i = 0; i < syncedThreads.length; i++) {
      syncedThreads[i] = new SynchedIncrementor();
    }
    for (Thread t : syncedThreads) {
      t.start();
    }
    syncedThreads[0].join();
    syncedThreads[1].join();
    System.out.println("'Synced' increment two million times gives :" + incrementMeSynched);
    
    Thread[] moreThreads = new Thread[2];
    for (int i = 0; i < moreThreads.length; i++) {
      moreThreads[i] = new Thread() {
        public void run() {
          for (int i = 0; i < 1000000;i++) {
            SimpleSync.increment();
          }
        }
      };
    }
    for (Thread t : moreThreads) {
      t.start();
    }
    moreThreads[0].join();
    moreThreads[1].join();
    System.out.println(
        "Synchronized increment two million times gives :" + incrementMeSynchedActually);
    
  }
  
  public synchronized static void increment() {
    incrementMeSynchedActually++;
  }
  
  private static class UnsynchedIncrementor extends Thread {
    public void increment() {
      incrementMeUnsynched++;
    }
    
    public void run() {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
    }
  }
  
  private static class SynchedIncrementor extends Thread {
    public synchronized void increment() {
      incrementMeSynched++;
    }
    
    public void run() {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
    }
  }
  
}
