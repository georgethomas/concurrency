package concurrency;

/**
 * This program prints "Hello world" from five separate threads.
 * The order in which the threads run is non-deterministic.
 * 
 * "Write a program that prints 'hello world' from five different threads.
 * Ensure the printed string to include the thread number; 
 * ensure that all threads have a unique thread number."
 */

public class CreatingThreads {

  public static void main(String[] args) {    
    Thread[] threads = new Thread[5];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new myThread(i);
    }
    for (Thread t : threads) {
      t.start();
    }
  }
  
  private static class myThread extends Thread {
    final int threadNumber;
    
    public myThread(int threadNumber) {
      this.threadNumber = threadNumber;
    }
    
    public void run() {
      System.out.println(String.format("Hello world form Thread %s!", threadNumber));
    }
  }
  
}
